from django.db import models

# Create your models here.


class OnsiteData(models.Model):
    DO = models.CharField(max_length = 100)
    Temperature = models.IntegerField()
    PH = models.FloatField()
    Stream_observation = models.CharField(max_length = 200)

    def __str__(self):
        return self.DO



class LabtestData(models.Model):
    SampleID = models.CharField(max_length =100)
    LabID = models.IntegerField(null=True)
    Project_name = models.CharField(max_length = 50)
    Collect_date = models.DateField()
    Ecoli = models.FloatField()
    Nitrate = models.FloatField()
    Nitrite = models.FloatField()
    Phosphorous = models.FloatField()
    Calcium = models.FloatField()
    Copper = models.FloatField()
    Lead = models.FloatField()
    Hardness_COco3 = models.FloatField()
    noN = models.FloatField()
    TSS = models.IntegerField()
    Excelfield = models.FileField(upload_to= 'Excelfile/')

    def __str__(self):
        return self.SampleID