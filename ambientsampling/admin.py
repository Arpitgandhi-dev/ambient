from django.contrib import admin
from ambientsampling.models import OnsiteData, LabtestData
# Register your models here.

admin.site.register(OnsiteData)
admin.site.register(LabtestData)
