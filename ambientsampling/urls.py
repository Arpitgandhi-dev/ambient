from django.urls import path

from . import views

app_name = "ambientsampling"


urlpatterns = [
    path("", views.homepage, name = "homepage"),
    path("login", views.login_request, name="login"),
    path("Onsite", views.OnsiteDataprocessing, name = "onsite"),
    path("Lab", views.Excelsheetprocessing, name = "Excelprocessing")
]