from django.apps import AppConfig


class AmbientsamplingConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ambientsampling'
