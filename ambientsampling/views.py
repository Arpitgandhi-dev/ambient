from django.shortcuts import  render, redirect
from .forms import NewUserForm, OnsiteDataForm, LabtestDataform
from django.contrib.auth import login
from django.contrib import messages #import messages
from django.contrib.auth import login, authenticate #add this
from django.contrib.auth.forms import AuthenticationForm #add this
from .models import LabtestData

import os
import openpyxl
from  openpyxl import load_workbook
def homepage(request):
	return render(request=request, template_name='ambientsampling/base.html')

def register(request):
	if request.method == "POST":
		form = NewUserForm(request.POST)
		if form.is_valid():
			user = form.save()
			login(request, user)
			messages.success(request, "Registration successful." )
			return redirect("ambientsampling:homepage")
		messages.error(request, "Unsuccessful registration. Invalid information.")
	form = NewUserForm
	return render (request=request, template_name="ambientsampling/register.html", context={"register_form":form})

def login_request(request):
	if request.method == "POST":
		form = AuthenticationForm(request, data=request.POST)
		if form.is_valid():
			username = form.cleaned_data.get('username')
			password = form.cleaned_data.get('password')
			user = authenticate(username=username, password=password)
			if user is not None:
				login(request, user)
				messages.info(request, f"You are now logged in as {username}.")
				return redirect("ambientsampling:homepage")
			else:
				messages.error(request,"Invalid username or password.")
		else:
			messages.error(request,"Invalid username or password.")
	form = AuthenticationForm()
	return render(request=request, template_name="ambientsampling/login.html", context={"login_form":form})

def OnsiteDataprocessing(request):
	if request.method == "POST":
		Siteform = OnsiteDataForm(request.POST)
		if Siteform.is_valid():
			Sitedata = Siteform.save()
			messages.success(request, "data has been saved")
			return redirect("ambientsampling:homepage")
		messages.error(request, "Data Invalid")
	form = OnsiteDataForm
	return render (request=request, template_name="ambientsampling/Onsite.html", context={"OnsiteDataForm":form})
	
def Excelsheetprocessing(request):
	if request.method == "POST":
		Siteform_lab = LabtestDataform(request.POST, request.FILES)
		BASE = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
		mainfile = os.path.join(BASE, 'media/Excelfile/')
		workbook = load_workbook(mainfile+'20-260-9160_Gen_Env_Transposed-converted.xlsx')
		sh = workbook["Table 1"]
		SampleID_Export = sh["a5"].value
		LabID_Export = sh["m5"].value
		Project_name_Export = sh["f5"].value
		Collect_date_Export = sh["c5"].value
		Ecoli_Export = sh["a14"].value		
		Saving_data = LabtestData(SampleID = SampleID_Export, LabID = LabID_Export, Project_name = Project_name_Export, Collect_date = Collect_date_Export, Ecoli = Ecoli_Export, Nitrate = 4.0, Nitrite = 6.0, Phosphorous = 8.0, Calcium = 10.0, Copper = 12.0, Lead = 14.0, Hardness_COco3 = 16.0, noN = 18.0, TSS = 20.0)
		Saving_data.save()
		"""if Siteform_lab.is_valid():
			site_lab = Siteform_lab.save()"""
		return redirect("ambientsampling:homepage")
		messages.error(request, "Data Invalid")
	form = LabtestDataform()
	return render (request=request, template_name="ambientsampling/Labdata.html", context={"LabtestData":form})


def Excelparsing(request):
	"""BASE = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
	mainfile = os.path.join(BASE, 'media/Excelfile/')
	workbook = load_workbook(mainfile+'20-260-9160_Gen_Env_Transposed-converted.xlsx')
	sh = workbook["Table 1"]"""
	SampleID_ = LabtestData(SampleID = 545, LabID = 983, Project_name = 'lOCHA', Collect_date = '2021-06-30', Ecoli = 2.0, Nitrate = 4.0, Nitrite = 6.0, Phosphorous = 8.0, Calcium = 10.0, Copper = 12.0, Lead = 14.0, Hardness_COco3 = 16.0, noN = 18.0, TSS = 20.0)
	SampleID_.save()
	return render (request=request, template_name="ambientsampling/Labdata.html")
