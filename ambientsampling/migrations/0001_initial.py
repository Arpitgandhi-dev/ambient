# Generated by Django 3.2.4 on 2021-06-29 16:36

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='OnsiteData',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('DO', models.CharField(max_length=100)),
                ('Temperature', models.IntegerField()),
                ('PH', models.FloatField()),
                ('Stream_observation', models.CharField(max_length=200)),
            ],
        ),
    ]
